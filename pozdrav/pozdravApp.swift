//
//  pozdravApp.swift
//  pozdrav
//
//  Created by admin on 21.10.2022.
//

import SwiftUI

@main
struct pozdravApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
